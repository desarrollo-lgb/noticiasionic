import { Component, OnInit } from '@angular/core';
import { ActionSheetController, ToastController } from '@ionic/angular';
import { RootObject } from '../interfaces/interfaces';
import { NoticiasService } from '../services/noticias.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  noticias: RootObject = {
    status: "",
    articles: [],
    totalResults: 0
  }

  constructor(private service: NoticiasService,
              public actionSheetController: ActionSheetController,
              public toastController: ToastController,
              private socialSharing: SocialSharing) {}

  ngOnInit(): void {
    this.getNoticiasFromService();
  }

  getNoticiasFromService() {
    this.service.getNoticias().subscribe(resp => {
      this.noticias = resp
      console.log(resp);
    })
  }

  async doRefresh(event) {
    console.log('Comienza a cargar las noticias de nuevo');

    await this.getNoticiasFromService();

    event.target.complete();
  }

  async presentActionSheet(noticia) {
    console.log(noticia);
    const actionSheet = await this.actionSheetController.create({
      cssClass: 'my-custom-class',
      buttons: [
        {
        text: 'Compartir',
        icon: 'share',
        handler: () => {
          this.shareNoticia(noticia)
        }
      }, {
        text: 'Guardar',
        icon: 'star',
        handler: () => {
          this.guardarNoticia(noticia);
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();

    const { role } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async guardarNoticia(noticia) {
    await this.service.saveNoticia(noticia);
    this.showMessage("Noticia agregada a favoritos");
  }

  shareNoticia(noticia) {
    this.socialSharing.share(noticia.title, noticia.urlToImage, noticia.url);
  }

  async showMessage(texto) {
    const toast = await this.toastController.create({
      message: texto,
      duration: 2000
    });
    toast.present();
  }

}
