import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RootObject } from '../interfaces/interfaces';

import { Storage } from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
  apikey: String = '33186f3250bb4e44954dbcc72b52cc7d'

  constructor(private http: HttpClient,
              private storage: Storage) { 
                this.init();
              }

  
  async init() {
    // If using, define drivers here: await this.storage.defineDriver(/*...*/);
    const storageInit = await this.storage.create();
    this.storage = storageInit;
  }

  getNoticias() {
    return this.http.get<RootObject>("https://newsapi.org/v2/everything?q=android&from=2021-06-30&sortBy=publishedAt&apiKey=" + this.apikey)
  }

  saveNoticia(objNoticia) {
    this.storage?.set(objNoticia.url, JSON.stringify(objNoticia));
  }

  async getFavoritas() {
    let noticias = []
    await this.storage.forEach((value, key, index) => {
      console.log(value);
      noticias.push(JSON.parse(value));
    });

    return await new Promise( resolve => {
      resolve(noticias)
    })
  }
}
