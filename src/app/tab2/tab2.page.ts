import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { NoticiasService } from '../services/noticias.service';
import { Article } from '../interfaces/interfaces';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  noticias: Article[]

  constructor(private service: NoticiasService,
              public actionSheetController: ActionSheetController) {}


  ionViewWillEnter(): void {
    this.traerNoticias();
  }

  traerNoticias() {
    this.service.getFavoritas()
      .then( (resp: Article[]) => {
        this.noticias = resp
      });
  }

}
